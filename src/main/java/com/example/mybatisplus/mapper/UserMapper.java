package com.example.mybatisplus.mapper;

import com.example.mybatisplus.anno.MyMapper;
import com.example.mybatisplus.entity.User;

import java.util.List;

public interface UserMapper {
    public List<User> select(User user);

    public Integer insert(User user);
}
