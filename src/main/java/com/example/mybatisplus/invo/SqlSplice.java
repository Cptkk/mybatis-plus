package com.example.mybatisplus.invo;

import com.example.mybatisplus.entity.User;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.*;

public class SqlSplice {
    private final static String SELECT_BEFORE = "SELECT * FROM user";
    private final static String INSERT_BEFORE = "INSERT INTO user";

    public static List<User> select(Object[] args) throws Exception {
        List<User> userList = new ArrayList<>();
        User user = (User) args[0];
        StringBuilder SQL = new StringBuilder();
        SQL.append(SELECT_BEFORE);
        SQL.append(getOption(user));
        System.out.println(SQL);
        ResultSet set = select(String.valueOf(SQL));
        while (set.next()) {
            userList.add(new User(set.getInt("id"), set.getString("name"), set.getString("desc")));
        }
        return userList;
    }

    public static Integer insert(Object[] args) throws Exception {
        StringBuilder SQL = new StringBuilder();
        User user = (User) args[0];
        SQL.append(INSERT_BEFORE);
        SQL.append(getInsertOption(user));
        System.out.println(SQL);
        return insert(String.valueOf(SQL));
    }

    public static ResultSet select(String sql) throws Exception {
        Connection conn = getConnection();
        //3.操作数据库，实现增删改查
        Statement stmt = conn.createStatement();
        return stmt.executeQuery(sql);
    }

    public static Integer insert(String sql) throws Exception {
        Connection conn = getConnection();
        //3.操作数据库，实现增删改查
        Statement stmt = conn.createStatement();
        return stmt.executeUpdate(sql);
    }

    public static Connection getConnection() {
        Connection conn = null;
        try {
            Class.forName("com.mysql.cj.jdbc.Driver");
            //2. 获得数据库连接
            conn = DriverManager.getConnection("jdbc:mysql://81.68.81.211:3306/my", "root", "Aa-123456");
        } catch (Exception e) {
            e.printStackTrace();
        }
        return conn;
    }

    public static StringBuilder getOption(User user) {
        StringBuilder OPTION = new StringBuilder();
        Map<String, String> map = new HashMap<>();
        map.put("id", String.valueOf(user.getId()));
        map.put("name", user.getName());
        map.put("desc", user.getDesc());

        Iterator<Map.Entry<String, String>> entries = map.entrySet().iterator();
        boolean flag = true;
        while (entries.hasNext()) {
            Map.Entry<String, String> entry = entries.next();
            if (entry.getValue() != null && !entry.getValue().equals("") && flag) {
                OPTION.append(" WHERE " + entry.getKey() + "='" + entry.getValue() + "'");
                flag = false;
            } else if (entry.getValue() != null && !entry.getValue().equals("null") && !entry.getValue().equals("") && !flag) {
                OPTION.append(" AND " + entry.getKey() + "='" + entry.getValue() + "'");
            }
        }
        return OPTION;
    }

    public static StringBuilder getInsertOption(User user) {
        StringBuilder OPTION = new StringBuilder();
        //涉及到序列化的问题，暂不处理
        OPTION.append(" values(" + user.getId() + ",'" + user.getName() + "','" + user.getDesc() + "')");
        return OPTION;
    }
}