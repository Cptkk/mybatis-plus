package com.example.mybatisplus.invo;

import com.baomidou.mybatisplus.core.MybatisSqlSessionFactoryBuilder;
import com.baomidou.mybatisplus.extension.spring.MybatisSqlSessionFactoryBean;
import com.example.mybatisplus.MybatisPlusApplication;
import com.example.mybatisplus.entity.User;

import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Method;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

public class UserInvocationHandler implements InvocationHandler {
    @Override
    public Object invoke(Object proxy, Method method, Object[] args) throws Throwable {
        return run(method, args);
    }

    public Object run(Method method, Object[] args) throws Exception {
        List<User> userList = new ArrayList<>();

        if (method.getName().equals("select")) {
            return SqlSplice.select(args);
        } else if (method.getName().equals("insert")) {
            return SqlSplice.insert(args);
        }
        return null;
    }
}
