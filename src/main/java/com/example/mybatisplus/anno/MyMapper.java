package com.example.mybatisplus.anno;

import java.lang.annotation.*;

@Documented  //生成javadoc的时候是否会被记录
@Inherited  //子类也会继承这个注解
@Target(ElementType.FIELD)
@Retention(RetentionPolicy.RUNTIME)
public @interface MyMapper {
    //--------------to be continued
}
