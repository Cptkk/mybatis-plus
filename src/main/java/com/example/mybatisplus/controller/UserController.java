package com.example.mybatisplus.controller;

import com.example.mybatisplus.entity.User;
import com.example.mybatisplus.invo.UserInvocationHandler;
import com.example.mybatisplus.mapper.UserMapper;
import org.springframework.web.bind.annotation.*;

import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Proxy;
import java.util.List;

@RestController
@RequestMapping("/user")
public class UserController {
    private final UserMapper userMapper = getUserMapper();

    //使用动态代理的方式，实现userMapper的select方法
    @GetMapping("/select")
    public List<User> select(@RequestBody User user) {
        return userMapper.select(user);
    }

    @PostMapping("/insert")
    public Integer insert(@RequestBody User user) {
        //调用方法时会调用调用处理器的invoke方法进行处理
        return userMapper.insert(user);
    }

    public static UserMapper getUserMapper() {
        //该处UserMapper的生成应该使用Spring的Autowired实现,暂无法自动实现
        //首先需要获得User对象的调用处理器，主要需要实现invoke()方法
        InvocationHandler invocationHandler = new UserInvocationHandler();
        //将调用处理器作为参数传递给动态代理，生成动态代理对象
        return (UserMapper) Proxy.newProxyInstance(UserMapper.class.getClassLoader(), new Class[]{UserMapper.class}, invocationHandler);
    }
}
